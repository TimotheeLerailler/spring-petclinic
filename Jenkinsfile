pipeline {
    agent any
    
    stages {
        stage('Checkout') {
            steps {
                checkout scm
                script {
                    def pom = readMavenPom file: 'pom.xml'
                    if (pom) {
                        echo "Building version ${pom.version}"
                    }
                    sh "chmod +x ./mvnw"
                }
            }
        }
        
        stage('Build') {
            steps {
                withMaven(jdk: 'Java 21', maven: 'Maven 3.9.6') {
                    sh "mvn compile"
                }
            }
        }
        
        stage('Test') {
            steps {
                withMaven(jdk: 'Java 21', maven: 'Maven 3.9.6') {
                    sh "mvn test"
                }
            }
        }
        
        stage('SonarQube analysis') {
            steps {
                withSonarQubeEnv('SonarCloud') {
                    sh "mvn clean package sonar:sonar"
                }
            }
        }
        
        stage('Quality Gate') {
            steps {
                timeout(time: 1, unit: 'HOURS') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
    }
}
